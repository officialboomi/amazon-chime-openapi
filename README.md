# Amazon Chime Connector
The Amazon Chime API (application programming interface) is designed for developers to perform key tasks, such as creating and managing Amazon Chime accounts, users, and Voice Connectors. 

Documentation: https://docs.aws.amazon.com/chime/

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/chime/2018-05-01/openapi.yaml

## Prerequisites

+ An Amazon Chime account.
+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

